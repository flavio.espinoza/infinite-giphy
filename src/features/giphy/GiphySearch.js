import React, { useContext, useState } from 'react';
import {
  Grid, // our UI Component to display the results
  SearchBar, // the search bar the user will type into
  SearchContext, // the context that wraps and connects our components
  SearchContextManager, // the context manager, includes the Context.Provider
} from '@giphy/react-components';
import ResizeObserver from 'react-resize-observer';
import './giphy.css';

// define the components in a separate function so we can
// use the context hook. You could also use the render props pattern
const Components = () => {
  const { fetchGifs, searchKey } = useContext(SearchContext);
  const [width, setWidth] = useState(window.innerWidth);
  const [columns, setColumns] = useState(1);
  return (
    <div>
      <div className={'giphy-search'}>
        <SearchBar />
        <button className={'button-giphy--icon'} onClick={() => setColumns(1)} style={{color: columns === 1 ? 'hotpink' : ''}}>
          <i className="fad fa-th-list fa-lg"></i>
        </button>
        <button className={'button-giphy--icon'} onClick={() => setColumns(2)} style={{color: columns === 2 ? 'hotpink' : ''}}>
          <i className="fad fa-th-large fa-lg"></i>
        </button>
        <button className={'button-giphy--icon fa-lg'} onClick={() => setColumns(3)} style={{color: columns === 3 ? 'hotpink' : ''}}>
          <i className="fad fa-th"></i>
        </button>
      </div>
      <Grid
        key={searchKey}
        fetchGifs={fetchGifs}
        width={width}
        columns={columns}
        gutter={6}
      />
      <ResizeObserver
        onResize={({ width }) => {
          setWidth(width);
        }}
      />
    </div>
  );
};

// the search experience consists of the manager and its child components that use SearchContext
const GiphySearch = (props) => {
  const { apiKey } = props;
  return (
    <SearchContextManager apiKey={apiKey}>
      <Components />
    </SearchContextManager>
  );
};

export default GiphySearch;
