import React, { useState } from 'react';
import { GiphyFetch } from '@giphy/js-fetch-api';
import { Gif, Grid, Carousel } from '@giphy/react-components';
import { useAsync } from 'react-async-hook';
import ResizeObserver from 'react-resize-observer';

const giphyApiKey = 'CdRKiCMbTnt9CkZTZ0lGukSczk6iT4Z6';
const giphyFetch = new GiphyFetch(giphyApiKey);

function GridLayout({ onGifClick, query = 'dogs' }) {
  const fetchGifs = (offset) => giphyFetch.search(query, { offset, limit: 10 });
  const [width, setWidth] = useState(window.innerWidth);
  return (
    <div className="App">
      <Grid
        onGifClick={onGifClick}
        fetchGifs={fetchGifs}
        width={width}
        columns={3}
        gutter={6}
      />
      <ResizeObserver
        onResize={({ width }) => {
          setWidth(width);
        }}
      />
    </div>
  );
}

function GiphyApp() {
  const [modalGif, setModalGif] = useState();
  return (
    <div className="App">
      <h4>Grid</h4>
      <GridLayout
        onGifClick={(gif, e) => {
          console.log('gif', gif);
          e.preventDefault();
          setModalGif(gif);
        }}
      />
      {modalGif && (
        <div
          style={{
            position: 'fixed',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            background: 'rgba(0, 0, 0, .8)',
          }}
          onClick={(e) => {
            e.preventDefault();
            setModalGif(undefined);
          }}>
          <Gif gif={modalGif} width={200} />
        </div>
      )}
    </div>
  );
}

export default GiphyApp;
