import { Grid } from '@giphy/react-components';
import { GiphyFetch } from '@giphy/js-fetch-api';
import config from 'config/config';

const gf = new GiphyFetch(config.giphy.api.key);
export default function GiphyGrid(props) {
  return (<Grid width={800} columns={3} fetchGifs={gf.trending({offset: 10, limit: 20})} />)
}